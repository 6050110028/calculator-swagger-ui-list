package com.plusitsolution.training.basicws.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatorController {

	private List<String> dataNum = new ArrayList<>();

	@PutMapping("/Plus/{Number1}/{Number2}")
	public double addDataPlus(@PathVariable("Number1") double Num1,@PathVariable("Number2") double Num2) {
		double result1 = Num1+Num2;
		dataNum.add(Double.toString(Num1)+"+"+Double.toString(Num2)+"="+result1) ;
		return result1;
	}
	

	@PutMapping("/Minus/{Number1}/{Number2}")
	public double addDataMinus(@PathVariable("Number1") double Num1,@PathVariable("Number2") double Num2) {
		double result2 = Num1-Num2;
		dataNum.add(Double.toString(Num1)+"-"+Double.toString(Num2)+"="+result2) ;
		return result2;
	}
	
	@PutMapping("/Multiple/{Number1}/{Number2}")
	public double addDataMultiple(@PathVariable("Number1") double Num1,@PathVariable("Number2") double Num2) {
		double result3 = Num1*Num2;
		dataNum.add(Double.toString(Num1)+"x"+Double.toString(Num2)+"="+result3) ;
		return result3;
	}
	
	@PutMapping("/Divide/{Number1}/{Number2}")
	public double addDataDivided(@PathVariable("Number1") double Num1,@PathVariable("Number2") double Num2) {
		double result4 = Num1/Num2;
		dataNum.add(Double.toString(Num1)+"/"+Double.toString(Num2)+"="+result4) ;
		return result4;
	}
	//@DeleteMapping("/deleteMember")
	//public void deleteMember(@RequestParam("memberName") String memberName) {
		//dataNum.remove(memberName);
	//}

	@GetMapping("/listDataNum")
	public List<String> listDataNum() {
		return dataNum;
	}

}
